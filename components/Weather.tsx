import axios from 'axios';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { StyleSheet } from 'react-native';
import { Text } from 'react-native';
import { View } from './Themed';

export function Weather() {
  const [weatherData, setWeatherData] = useState({
    main: '',
    description: ''
  })
  useEffect(() => {

    axios({
      method: 'get',
      url: 'http://api.openweathermap.org/data/2.5/weather?id=2172797&appid=015ef105d750627a35a50675ec38d07f'
    })
      .then(function (response) {
        console.log(JSON.stringify(response.data.weather[0]));
        setWeatherData(response.data.weather[0])
      })
      .catch(function (error) {
        console.log(error);
      });
  }, [])
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{weatherData.main}</Text>
      <Text style={styles.subtitle}>{weatherData.description}</Text>
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white'
  },
  subtitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#333388',
    marginTop: 10
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
